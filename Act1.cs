//Crear una clase Persona que tenga como atributos el "cedula, nombre, apellido y la edad (definir las propiedades para poder acceder a dichos atributos)". Definir como responsabilidad un método para mostrar ó imprimir. Crear una segunda clase Profesor que herede de la clase Persona. Añadir un atributo sueldo ( y su propiedad) y el método para imprimir su sueldo. Definir un objeto de la clase Persona y llamar a sus métodos y propiedades. También crear un objeto de la clase Profesor y llamar a sus métodos y propiedades.

using System;

class Persona 
{
  public string cedula, nombre, apellido;
  public int edad;

  public void mostrar()
  {
    Console.WriteLine("**Persona**");
    Console.WriteLine("Cedula: {0}" ,cedula);
    Console.WriteLine("Nombre: {0}" ,nombre);
    Console.WriteLine("Apellido: {0}" ,apellido);
    Console.WriteLine("Edad: {0}",edad);
    Console.WriteLine(" ");
  }
  public class Profesor
  {
    public string cedula1, nombre1, apellido1;
    public int edad1, sueldo;

    public void mostrar1()
    {
      Console.WriteLine("**Profesor**");
      Console.WriteLine("Cedula: {0}" ,cedula1);
      Console.WriteLine("Nombre: {0}" ,nombre1);
      Console.WriteLine("Apellido: {0}" ,apellido1);
      Console.WriteLine("Edad: {0}",edad1);
      Console.WriteLine("Sueldo Mensual: RD$ {0}" ,sueldo);
      Console.ReadKey();
    }
  }

  public static void Main (string[] args) 
  {
    Persona p = new Persona();
    p.cedula= "0-54385-2";
    p.nombre= "Alex";
    p.apellido= "Fernandez";
    p.edad= 21;
    p.mostrar(); 

    Profesor pr= new Profesor();
    pr.cedula1= "0-48139-4";
    pr.nombre1= "Luis";
    pr.apellido1= "Encarnacion";
    pr.edad1= 43;
    pr.sueldo= 15000;
    pr.mostrar1();

  }
}