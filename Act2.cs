//Crear una clase Contacto. Esta clase deberá tener los atributos "nombre, apellidos, telefono y direccion". También deberá tener un método "SetContacto", de tipo void y con los parámetros string, que permita cambiar el valor de los atributos. También tendrá un método "Saludar", que escribirá en pantalla "Hola, soy " seguido de sus datos. Crear también una clase llamada ProbarContacto. Esta clase deberá contener sólo la función Main, que creará dos objetos de tipo Contacto, les asignará los datos del contacto y les pedirá que saluden.

using System;

class MainClass 
{
  
  public class Contacto
  {
    public string nombre, apellidos, telefono, direccion;

    public void SetContacto(string nombre)
    {
      this.nombre= nombre;
    }
    public void SetContacto1(string apellidos)
    {
      this.apellidos= apellidos;
    }
    public void SetContacto2(string telefono)
    {
      this.telefono= telefono;
    }
    public void SetContacto3(string direccion)
    {
      this.direccion= direccion;
    }

    public void saludar()
    {
      Console.WriteLine("Hola!, soy {0} {1} , mi numero es el {2} y vivo en {3}" ,nombre, apellidos, telefono, direccion);
      Console.WriteLine(" ");

    }


  }
  public class ProbarContacto
  {
    public static void Main () 
    {
      Contacto c= new Contacto();
      c.SetContacto("Alfredo");
      c.SetContacto1("Rodriguez Beato");
      c.SetContacto2("123-456-7891");
      c.SetContacto3("la Calle las flautas No.3");

      Contacto c2= new Contacto();
      c2.SetContacto("Laura");
      c2.SetContacto1("Peguero");
      c2.SetContacto2("198-765-4321");
      c2.SetContacto3("Los tres ojos");

      c.saludar();
      c2.saludar();
      Console.ReadKey();
      
    }
  }
}