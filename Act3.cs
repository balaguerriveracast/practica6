//Crear tres clases ClaseA, ClaseB, ClaseC que ClaseB herede de ClaseA y ClaseC herede de ClaseB. Definir un constructor a cada clase que muestre un mensaje. Luego definir un objeto de la clase Clase C.

using System;

class Main
{
  public class ClaseA
  {
    public ClaseA(int a)
    {
      Console.WriteLine("Clase A: " +a);
    }
  }
  public class ClaseB : ClaseA 
  {
    public ClaseB(int b):base(b/3)
    {
      Console.WriteLine("Clase B: "+b);
    }
  }
  public class ClaseC : ClaseB
  {
    public ClaseC(int c):base(c/3)
    {
      Console.WriteLine("Clase C: "+c);
      Console.ReadKey();
    }
  }
  
  class principal
  {
    static void Main (string[] args) 
    {
      ClaseC clases= new ClaseC(15);
    }
  }
}